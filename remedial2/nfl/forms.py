from django import forms
from .models import Ciudad, Equipo, Estadio,Duenio, Jugador

class CiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ["nombre"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de la ciudad"}),
        }

class EquipoForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = ["nombre", "ciudad", "duenio"]  
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del equipo"}),
            "ciudad": forms.Select(attrs={"class": "form-control"}),
            "duenio": forms.Select(attrs={"class": "form-control"}),  
        }

class EstadioForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = ["nombre", "equipo", "duenio","capacidad","size"]  
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del estadio"}),
            "equipo": forms.Select(attrs={"class": "form-control"}),
            "duenio": forms.Select(attrs={"class": "form-control"}),  
            "capacidad": forms.TextInput(attrs={"class": "form-control"}),
            "size": forms.TextInput(attrs={"class": "form-control"}),
        }

class JugadorForm(forms.ModelForm):
    class Meta:
        model = Jugador
        fields = ["nombre", "equipo","numero","posicion"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del jugador"}),
            "equipo": forms.Select(attrs={"class": "form-control"}),
            "numero": forms.TextInput(attrs={"class": "form-control"}),
            "posicion": forms.TextInput(attrs={"class": "form-control"}),
        }

class DuenioForm(forms.ModelForm):
    class Meta:
        model = Duenio
        fields = ["nombre"]
        widgets = {
            "nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del dueño"}),
        }